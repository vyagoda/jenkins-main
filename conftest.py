import os

import pytest


@pytest.fixture(scope='session', autouse=True)
def env():
    yield
    with open(f"{os.getenv('PWD')}/some.json", 'w') as f:
        f.write('123')
